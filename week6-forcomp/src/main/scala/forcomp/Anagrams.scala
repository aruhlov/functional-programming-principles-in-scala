package forcomp

import common._

object Anagrams {

  type Word = String

  type Sentence = List[Word]

  type Occurrences = List[(Char, Int)]

  val dictionary: List[Word] = loadDictionary

  def wordOccurrences(w: Word): Occurrences = (for {
        (c, s) <- w.groupBy(_.toLower)
      } yield (c, s.length)).toList.sortBy(_._1)

  def sentenceOccurrences(s: Sentence): Occurrences = wordOccurrences(s.mkString)

  /**
   *
   */
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] =
    dictionary.groupBy(wordOccurrences)

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] =
    dictionaryByOccurrences(wordOccurrences(word))

  /**
   *
   * @param occurrences
   * @return
   */
  def combinations(occurrences: Occurrences): List[Occurrences] =
    occurrences.foldRight(List[Occurrences](List())) {
      case ((c, size), rest) => (for {combs <- rest; len <- 1 to size} yield (c, len) :: combs) ++ rest
    }

  /**
   *
   * @param x
   * @param y
   * @return
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = y.foldLeft(x.toMap){
    case (map, (c, size)) =>
      if (map(c) - size <= 0) map - c
      else map.updated(c, map(c) - size)
  }.toList.sorted

  /**
   *
   * @param sentence
   * @return
   */
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = {
    def iter(scc: Occurrences) : List[Sentence] =
      scc match {
        case Nil => List(List())
        case _   => for {
          cs <- combinations(scc)
          ws <- dictionaryByOccurrences(cs)
          rest <- iter(subtract(scc, wordOccurrences(ws)))
        } yield ws :: rest
      }

    iter(sentenceOccurrences(sentence))
  }

}
