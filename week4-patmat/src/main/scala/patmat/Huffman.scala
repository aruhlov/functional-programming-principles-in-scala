package patmat

import common._

/**
 *
 */
object Huffman {

  abstract class CodeTree
  case class Fork(left: CodeTree, right: CodeTree, chars: List[Char], weight: Int) extends CodeTree
  case class Leaf(char: Char, weight: Int) extends CodeTree

  /**
   *
   * @param tree
   * @return
   */
  def weight(tree: CodeTree): Int = tree match {
    case Fork(_, _, _, w) => w
    case Leaf(_, w) => w
  }

  /**
   *
   * @param tree
   * @return
   */
  def chars(tree: CodeTree): List[Char] = tree match {
    case Fork(_, _, c, _) => c
    case Leaf(c, _) => List(c)
  }

  /**
   *
   * @param left
   * @param right
   * @return
   */
  def makeCodeTree(left: CodeTree, right: CodeTree) =
    Fork(left, right, chars(left) ::: chars(right), weight(left) + weight(right))

  /**
   *
   * @param str
   * @return
   */
  def string2Chars(str: String): List[Char] = str.toList

  /**
   *
   * @param chars
   * @return
   */
  def times(chars: List[Char]): List[(Char, Int)] = {

    def iter(left: List[Char], seen: List[Char], acc : List[(Char, Int)]) : List[(Char, Int)] =
      if (left.isEmpty) acc
      else if (seen.contains(left.head)) iter(left.tail, seen, acc)
      else iter(left.tail, seen ++ List(left.head), List((left.head, letCount(left.head, chars))) ++ acc)

    iter(chars, List(), List())
  }

  /**
   *
   * @param let
   * @param letters
   * @return
   */
  def letCount(let: Char, letters: List[Char]): Int = {

    def iter(accLetters: List[Char], accLet: Char, acc: Int): Int =
      if (accLetters.isEmpty) acc
      else if (accLetters.head == accLet) iter(accLetters.tail, accLet, acc + 1)
      else iter(accLetters.tail, accLet, acc)

    iter(letters, let, 0)
  }

  /**
   *
   * @param freqs
   * @return
   */
  def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] = {
    def convertToLeft(left: List[(Char, Int)], acc: List[Leaf]): List[Leaf] =
      if (left.isEmpty) acc
      else {
        val current = left.head
        convertToLeft(left.tail, acc ++ List(Leaf(current._1, current._2)))
      }
    convertToLeft(freqs.sortWith((l1: (Char, Int), l2: (Char, Int)) => l1._2 < l2._2 ), List())
  }

  /**
   *
   * @param trees
   * @return
   */
  def singleton(trees: List[CodeTree]): Boolean = !trees.isEmpty && trees.tail.isEmpty

  /**
   *
   * @param trees
   * @return
   */
  def combine(trees: List[CodeTree]): List[CodeTree] = trees match {
    case tree :: Nil => trees
    case tree1 :: tree2 :: right => (makeCodeTree(tree1, tree2) :: right).sortWith(
      (ct1: CodeTree, ct2: CodeTree) => weight(ct1) < weight(ct2)
    )
  }

  type TreeList = List[CodeTree]
  type BitList = List[Bit]

  /**
   *
   * @param isSingleton
   * @param combine
   * @param trees
   * @return
   */
  def until(isSingleton: (TreeList) => Boolean, combine: (TreeList) => TreeList)(trees: TreeList): TreeList =
    if (isSingleton(trees)) trees
    else until(isSingleton, combine)(combine(trees))

  /**
   *
   * @param chars
   * @return
   */
  def createCodeTree(chars: List[Char]): CodeTree =
    until(singleton, combine)(makeOrderedLeafList(times(chars))).head

  type Bit = Int

  /**
   *
   * @param tree
   * @param bits
   * @return
   */
  def decode(tree: CodeTree, bits: BitList): List[Char] = {
    def iter(accTree: CodeTree, accBits: BitList, acc : List[Char]) : List[Char] = accTree match {
      case Leaf(c, _) => iter(tree, accBits, acc ++ List(c))
      case Fork(left, right, _, _) =>
        if (accBits.isEmpty) acc
        else if (accBits.head == 0) iter(left, accBits.tail, acc)
        else iter(right, accBits.tail, acc)
    }

    iter(tree, bits, List())
  }

  val frenchCode: CodeTree = Fork(Fork(Fork(Leaf('s',121895),Fork(Leaf('d',56269),Fork(Fork(Fork(Leaf('x',5928),Leaf('j',8351),List('x','j'),14279),Leaf('f',16351),List('x','j','f'),30630),Fork(Fork(Fork(Fork(Leaf('z',2093),Fork(Leaf('k',745),Leaf('w',1747),List('k','w'),2492),List('z','k','w'),4585),Leaf('y',4725),List('z','k','w','y'),9310),Leaf('h',11298),List('z','k','w','y','h'),20608),Leaf('q',20889),List('z','k','w','y','h','q'),41497),List('x','j','f','z','k','w','y','h','q'),72127),List('d','x','j','f','z','k','w','y','h','q'),128396),List('s','d','x','j','f','z','k','w','y','h','q'),250291),Fork(Fork(Leaf('o',82762),Leaf('l',83668),List('o','l'),166430),Fork(Fork(Leaf('m',45521),Leaf('p',46335),List('m','p'),91856),Leaf('u',96785),List('m','p','u'),188641),List('o','l','m','p','u'),355071),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u'),605362),Fork(Fork(Fork(Leaf('r',100500),Fork(Leaf('c',50003),Fork(Leaf('v',24975),Fork(Leaf('g',13288),Leaf('b',13822),List('g','b'),27110),List('v','g','b'),52085),List('c','v','g','b'),102088),List('r','c','v','g','b'),202588),Fork(Leaf('n',108812),Leaf('t',111103),List('n','t'),219915),List('r','c','v','g','b','n','t'),422503),Fork(Leaf('e',225947),Fork(Leaf('i',115465),Leaf('a',117110),List('i','a'),232575),List('e','i','a'),458522),List('r','c','v','g','b','n','t','e','i','a'),881025),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u','r','c','v','g','b','n','t','e','i','a'),1486387)

  val secret: BitList = List(0,0,1,1,1,0,1,0,1,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,1,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,0,1,0,1,1,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,1)

  /**
   *
   * @return
   */
  def decodedSecret: List[Char] = decode(frenchCode, secret)


  /**
   *
   * @param tree
   * @param text
   * @return
   */
  def encode(tree: CodeTree)(text: List[Char]): BitList = {
    def iter(accTree: CodeTree, accText: List[Char], acc: BitList): BitList = {
      if (accText.isEmpty) acc else accTree match {
        case Leaf(_, _) => iter(tree, accText.tail, acc)
        case Fork(left, right, _, _) =>
          if (chars(left).contains(accText.head)) iter(left, accText, acc ++ List(0))
          else if (chars(right).contains(accText.head)) iter(right, accText, acc ++ List(1))
          else List()
      }
    }

    iter(tree, text, List())
  }

  type CodeTable = List[(Char, BitList)]

  /**
   *
   * @param table
   * @param char
   * @return
   */
  def codeBits(table: CodeTable)(char: Char): BitList = {
    val f = table.find((t) => t._1 == char)
    if (f != None) f.head._2
    else List()
  }

  /**
   *
   * @param tree
   * @return
   */
  def convert(tree: CodeTree): CodeTable = {
    def iter(accTree : CodeTree, acc : BitList) : CodeTable = accTree match {
      case leaf: Leaf => List((leaf.char, acc))
      case fork: Fork =>
        mergeCodeTables(iter(fork.left, acc ++ List(0)), iter(fork.right, acc ++ List(1)))
    }
    iter(tree, List())
  }

  /**
   *
   * @param a
   * @param b
   * @return
   */
  def mergeCodeTables(a: CodeTable, b: CodeTable): CodeTable = a ++ b

  /**
   *
   * @param tree
   * @param text
   * @return
   */
  def quickEncode(tree: CodeTree)(text: List[Char]): BitList = {
    def iter(accTable: CodeTable, accText: List[Char], acc: BitList): BitList =
      if (accText.isEmpty) acc
      else iter(accTable, accText.tail, acc ++ codeBits(accTable)(accText.head))
    iter(convert(tree), text, List())
  }
}
