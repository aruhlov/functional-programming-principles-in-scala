# README #

Coursera accomplished of the Principles of Functional Programming in Scala class!

[Source](https://bitbucket.org/aruhlov/functional-programming-principles-in-scala/src)

[Certificate](https://bitbucket.org/aruhlov/functional-programming-principles-in-scala/src/7c84a05b32e35c86665ed7c0f45ef4500c89db78/coursera-progfun-certificate-2014.pdf?at=master)