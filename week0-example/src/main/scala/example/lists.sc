val vector = List(1, 2, 3, 4)

vector.head
vector.tail

def sum(xs: List[Int]): Int = {
  if (xs.isEmpty) 0
  else xs.head + sum(xs.tail)
}
sum(List(1, 2, 3, 4))
assert(sum(List(1, 2, 3, 4)) == 10)